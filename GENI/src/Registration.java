// Ryan Azoune
// Rym Bach
// Celina Ghoraieb-Munoz
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;


/**
 * Modèle d'une Inscription
 * 
 * @author
 */
public class Registration {

	private String date;
	private LocalDate service_date;
	private String pro_number;
	private String client_number;
	private String service_code;
	private String comments;
	
	/**
	 * Construction d'un modèle d'inscription.
	 * 
	 * @param service_date Date de la séance.
	 * @param pro_number Numéro du professionnel qui offre le service.
	 * @param client_number Numéro du client qui s'inscrit à la séance.
	 * @param service_code Numéro de service de la séance.
	 * @param comments Commentaire.
	 */
	public Registration(LocalDate service_date, String pro_number, String client_number, String service_code, String comments) {
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = new Date();
		this.date = dateFormat.format(date);
		
		this.service_date = service_date;
		this.pro_number = pro_number;
		this.client_number = client_number;
		this.service_code = service_code;
		this.comments = comments;

	}

	
	/**
	 * Override de la méthode toString(), permet d'afficher correctement l'information de l'inscription.
	 * 
	 * @return String
	 */
	@Override
	public String toString() {
		String s = new String();
		s = "Date et heure actuelles : " + this.date + "\n"
				+ "Date à laquelle le service sera fourni : " + this.service_date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "\n"
				+ "Numéro du professionnel : " + this.pro_number + "\n"
				+ "Numéro du membre : " + this.client_number + "\n"
				+ "Code du service : " + this.service_code + "\n"
				+ "Commentaire : " + this.comments;
		return s;
	}

}
