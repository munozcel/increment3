// Ryan Azoune
// Rym Bach
// Celina Ghoraieb-Munoz
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Interface du menu principal.
 * 
 * @author
 */

public class Menu  {
	private static Scanner s = new Scanner(System.in);
	/**
	 * Retourne au menu principal quand l'agent appuie sur "Entrée".
	 */
	public final static void returnMainMenu() {	
		System.out.println("\nAppuyez sur la touche entrée pour revenir au menu principal.");
		s.nextLine();
		mainMenu();
		return;
	}

	/**
	 * Efface l'écran du terminal.
	 */
	public final static void clearScreen() {
		for(int i = 0; i < 100; i++) {
			System.out.println();
		}
	}

	/**
	 * Retourne l'entrée de l'agent correspondant à un nombre entre un minimum et un maximum, et
	 * retourne une erreur en cas d'entrée invalide.
	 *
	 * @param min Valeur minimale.
	 * @param max Valeur maximale.
	 * @param withReturn True si "-1" doit être retournée l'orsque aucune réponse n'est entrée.
	 *
	 * @return int Valeur numérique du choix une fois "sanitisé".
	 */
	public static int choseNum(int min,int max, boolean withReturn) {
		String  input;
		int choice = -1;

		while(choice < min || choice > max) {
			input = s.nextLine();

			try {
				choice = Integer.parseInt(input);
			} catch(Exception e){
				choice = -1;
			}

			if(withReturn && input.length() == 0) {
				return -1;
			}

			if(choice < min || choice > max) {
				System.out.println("Entrée invalide.");
			}
		}

		return choice;
	}

	/**
	 * Fonction qui effectue une pause.
	 * 
	 * @param ms Nombre de milisecondes à attendre.
	 */
	public final static void timeSleep(int ms) {
		try {
			TimeUnit.MILLISECONDS.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}



	
	/**
	 * Menu principal de l'application.
	 */
	public static void mainMenu() {
		clearScreen();
		
		int choice = -1;
		
		System.out.println("\n\n");
		System.out.println("------------------------------------------------------");
		System.out.println("Menu de l'application #GYM - veuillez entrer un nombre");
		System.out.println("------------------------------------------------------");
		System.out.println(" 1 : Accès au centre");
		System.out.println(" 2 : Création de compte");
		System.out.println(" 3 : Modification de compte");
		System.out.println(" 4 : Création de service");
		System.out.println(" 5 : Modification de service");
		System.out.println(" 6 : Inscription à une scéance");
		System.out.println(" 7 : Consultation des scéances du professionnel");
		System.out.println(" 8 : Confirmation d'inscription");
		System.out.println(" 9 : Rapport comptable pour le gérant");
		System.out.println("10 : Exécuter la procédure comptable");
		System.out.println("11 : Modifier l'état d'un membre (API pour RNB)");
		System.out.println(" 0 : Quitter");
		System.out.println("------------------------------------------------------\n");
		System.out.print("Choix : ");
		
		choice = choseNum(0, 11, false);
		
		switch(choice) {
			case 1:
				accesMenu();
				break;
			case 2:
				createAccountMenu();
				break;
			case 3:
				modifAccountMenu();
				break;
			case 4:
				createServicesMenu();
				break;
			case 5:
				modifServicesMenu();
				break;
			case 6:
				inscriptionService();
				break;
			case 7:
				consultationServicePro();
				break;
			case 8:
				confirmationInscription();
				break;
			case 9:
				rapportComptable();
				break;
			case 10:
				executeProcedureComptable();
				break;
			case 11:
				rnbAPI();
				break;
			case 0:
				break;
		}
		
		clearScreen();
		return;
	} 
	
	/**
	 * Accès au centre #GYM pour un membre (menu d'identification).
	 */
	public static void accesMenu() {
		clearScreen();
		
		System.out.println("Accès au GYM");
		System.out.println("À tout moment, veuillez appuyer sur entrée pour revenir en arrière\r\n");
		System.out.println("Veuillez entrer l'adresse courriel (connexion Facebook)");
		
		String input_code = " ";
		
		input_code = s.nextLine();
		
		User user = SystemControl.stateUserRegistration(input_code); 
		
		while (user == null) {
			if(input_code.length() == 0) {
				mainMenu();
				return;
			}
			
			System.out.println("Le compte est invalide. Veuillez réessayer ou appuyer sur entrée.");
			
			input_code = s.nextLine();
			user = SystemControl.stateUserRegistration(input_code);
		}
		
		if(user.getState())
			System.out.println("Le compte " + user + " est valide.\r\n[QR CODE D'ENTRÉE ICI]");
		else 
			System.out.println("Le compte " + user + " est suspendu.");
		
		returnMainMenu();
		return;
	}
	
		
	/**
	 * Menu de création d'un compte.
	 */
	public static void createAccountMenu() {
		clearScreen();
		
		System.out.println("Création de compte :");
		System.out.println("(Touche entrée = retour au menu)\n");
		
		String first_name, last_name, birthday, email, phone, address, city, province, postal_code;
		
		System.out.println("Veuillez S.V.P. entrer les informations suivantes :");
		
		System.out.println("Votre prénom ? (1 à 20 lettres)");
		first_name = s.nextLine();
		
		while(SystemControl.checkGeneralData("first_name", first_name) == false) {
			if(first_name.length() == 0) {
				mainMenu();
				return;
			}
			
			System.out.println("Prénom invalide. (1 à 20 lettres)");
			first_name = s.nextLine();
		}
		
		System.out.println("Votre nom de famille ? (1 à 25 lettres)");
		last_name = s.nextLine();
		
		while(SystemControl.checkGeneralData("last_name", last_name) == false) {
			if(last_name.length() == 0) {
				mainMenu();
				return;
			}
			
			System.out.println("Nom de famille invalide. (1 à 25 lettres)");
			last_name = s.nextLine();
		}
		
		System.out.println("Votre date de naissance au format JJ-MM-AAAA ? (+ de 5 ans)");
		birthday = s.nextLine();
		
		while(SystemControl.checkDataBirthDate(birthday) == false) {
			if(birthday.length() == 0) {
				mainMenu();
				return;
			}
			
			System.out.println("Date de naissance invalide. (JJ-MM-AAAA, + de 5 ans)");
			birthday = s.nextLine();
		}
		
		System.out.println("Votre adresse courriel ?");
		email = s.nextLine();
		
		while(SystemControl.checkGeneralData("email", email) == false) {
			if(email.length() == 0) {
				mainMenu();
				return;
			}
			
			System.out.println("Adresse courriel invalide.");
			email = s.nextLine();
		}
		
		System.out.println("Votre numéro de téléphone [2-9]XX-XXX-XXXX ?");
		phone = s.nextLine();
		
		while(SystemControl.checkGeneralData("phone", phone) == false) {
			if(phone.length() == 0) {
				mainMenu();
				return;
			}
			
			System.out.println("Numéro de téléphone invalide. [2-9]XX-XXX-XXXX");
			phone = s.nextLine();
		}
		
		System.out.println("Votre addresse postale ? (1 à 25 caractères)");
		address = s.nextLine();
		
		while(SystemControl.checkGeneralData("address", address) == false) {
			if(address.length() == 0) {
				mainMenu();
				return;
			}
			
			System.out.println("Adresse invalide. (1 à 25 caractères)");
			address = s.nextLine();
		}
		
		System.out.println("Votre ville ? (1 à 14 caractères)");
		city = s.nextLine();
		
		while(SystemControl.checkGeneralData("city", city) == false) {
			if(city.length() == 0) {
				mainMenu();
				return;
			}
			
			System.out.println("Ville invalide. (1 à 14 caractères)");
			city = s.nextLine();
		}
		
		System.out.println("Votre province ? (2 lettres)");
		province = s.nextLine();
		
		while(SystemControl.checkGeneralData("province", province) == false) {
			if(province.length() == 0) {
				mainMenu();
				return;
			}
			
			System.out.println("Province invalide. (2 lettres)");
			province = s.nextLine();
		}
		
		System.out.println("Votre code postal ? (6 caractères)");
		postal_code = s.nextLine();
		
		while(SystemControl.checkGeneralData("postal_code", postal_code) == false) {
			if(postal_code.length() == 0) {
				mainMenu();
				return;
			}
			
			System.out.println("Code postal invalide. (6 caractères)");
			postal_code = s.nextLine();
		}
		
		System.out.println("Est-ce un compte professionnel ? o/n");
		String input_pro, input_paiement, userCode;
		
		input_pro = s.nextLine();
		
		while(input_pro.length() == 0 || input_pro.length() > 1 || (input_pro.charAt(0) != 'o' && input_pro.charAt(0) != 'n')) {
			System.out.println("Entrée invalide.");
			input_pro = s.nextLine();
		}
		
		if(input_pro.charAt(0) == 'n') { 
			System.out.println("\nFrais d'adhésion : 50$.\n"
							 + "Entrer \"o\" quand le client a payé.\n"
							 + "Entrer \"n\" pour annuler si le client ne peut pas payer.");
			
			input_paiement = s.nextLine();
			
			while(input_paiement.length() == 0 || input_paiement.length() > 1 || (input_paiement.charAt(0) != 'o' && input_paiement.charAt(0) != 'n')) {
				System.out.println("Entrée invalide.");
				input_paiement = s.nextLine();
			}
			
			if(input_paiement.charAt(0) == 'n') {
				mainMenu();
				return;
			}
		}
		
		userCode = SystemControl.createAccount(first_name, last_name, birthday, email, phone, address, city, province, postal_code, input_pro.charAt(0) == 'o');
		
		if(userCode != null)
			System.out.println("Le code utilisateur est : " + userCode +"\n");
		else
			System.out.println("Le compte n'a pas pu être créé car il existe déjà un compte avec cette adresse email. \n");
			
		returnMainMenu();
		return;
	}
	
	/**
	 * Menu de modification de compte.
	 */
	public static void modifAccountMenu(){
		
		String phone ,email, address, city, province, postal_code;
		
		clearScreen();
		System.out.println("Modification de Compte:\n");
		System.out.println("Entrer le code/l'email d'identification du compte:");
		
		String  input_code = " ";
		
		input_code = s.nextLine();
		User user = SystemControl.stateUserRegistration(input_code); 
		
		while (user == null) {
			if(input_code.length()==0) {
				mainMenu();
				return;
			}
			System.out.println("Le code entré est invalide. Entrer le code d'identification du compte:");
			input_code = s.nextLine();
			user = SystemControl.stateUserRegistration(input_code);
		}
		
		input_code = user.getNumber();
		
		System.out.println("\nPrénom : " + user.getFirstName() );
		System.out.println("Nom : " + user.getLastName() );
		System.out.println("Date de naissance : " + user.getBirthDate() );
		System.out.println("Email : " + user.getEmail() );
		System.out.println("Telephone : " + user.getPhone() );
		System.out.println("Adresse : " + user.getAddress() );
		System.out.println("Ville : " + user.getCity() );
		System.out.println("Province : " + user.getProvince() );
		System.out.println("Code postal : " + user.getPostalCode() );
		System.out.println("------------------------------------------\n");
		System.out.println("1 : Modifier les coordonnées.");
		System.out.println("2 : Suppression du compte.");
		System.out.println("0 : Retour");
		System.out.print("Choix :");
		
		int choice = choseNum(0,2,true);

		switch(choice) {
			case 0: // Quitter
				break;
				
			case 1: // Modification
				
				// Adresse email
				System.out.println("Adresse email?");
				email = s.nextLine();
				
				while( SystemControl.checkGeneralData("email", email)==false) {
					if(email.length() == 0) {
						mainMenu();
						return;
					}
					System.out.println("Email invalide");
					email = s.nextLine();
				}
				
				// Numéro de téléphone.
				System.out.println("Numéro de téléphone [2-9]XX-XXX-XXXX ?");
				phone = s.nextLine();
				
				while( SystemControl.checkGeneralData("phone", phone)==false) {
					if(phone.length() == 0) {
						mainMenu();
						return;
					}
					System.out.println("Numero invalide [2-9]XX-XXX-XXXX");
					phone = s.nextLine();
				}
				
				// Adresse postale.
				System.out.println("Votre addresse postale ? (1 à 25 caractères)");
				address = s.nextLine();
				
				while(SystemControl.checkGeneralData("address", address) == false) {
					if(address.length() == 0) {
						mainMenu();
						return;
					}
					
					System.out.println("Adresse invalide. (1 à 25 caractères)");
					address = s.nextLine();
				}
				
				// Ville
				System.out.println("Votre ville ? (1 à 14 caractères)");
				city = s.nextLine();
				
				while(SystemControl.checkGeneralData("city", city) == false) {
					if(city.length() == 0) {
						mainMenu();
						return;
					}
					
					System.out.println("Ville invalide. (1 à 14 caractères)");
					city = s.nextLine();
				}
				
				// Province
				System.out.println("Votre province ? (2 lettres)");
				province = s.nextLine();
				
				while(SystemControl.checkGeneralData("province", province) == false) {
					if(province.length() == 0) {
						mainMenu();
						return;
					}
					
					System.out.println("Province invalide. (2 lettres)");
					province = s.nextLine();
				}
				
				// Code postal
				System.out.println("Votre code postal ? (6 caractères)");
				postal_code = s.nextLine();
				
				while(SystemControl.checkGeneralData("postal_code", postal_code) == false) {
					if(postal_code.length() == 0) {
						mainMenu();
						return;
					}
					
					System.out.println("Code postal invalide. (6 caractères)");
					postal_code = s.nextLine();
				}
				
				if(SystemControl.editUser(email, phone, address, city, province, postal_code, input_code)) {
					System.out.println("La modification des informations s'est déroulée avec succès.");
				}else {
					System.out.println("La modification n'a pas pu être réalisée");
				}
				returnMainMenu();
				return;
			case 2: // Suppression
				
				System.out.println("Confirmation de supression ? o/n");
				
				String input_supr =s.nextLine();
				
				while(input_supr.length()==0||input_supr.length()>1 || (input_supr.charAt(0) != 'o' && input_supr.charAt(0) != 'n')) {
					System.out.println("Entré invalide");
					input_supr =s.nextLine();
				}
				
				if(input_supr.charAt(0) == 'o') { 
					if(SystemControl.deleteUser(input_code)) {
						System.out.println("La supression a été réalisée avec succès");
					}else {
						System.out.println("La supression n'a pas pu être réalisée");
					}
				}
				
				if(input_supr.charAt(0) == 'n') { 
					System.out.println("Supression annulée");
				}
				returnMainMenu();
				return;
		}
		
		clearScreen();
		mainMenu();
		return;
	}
	
	
	/**
	 * Menu de création de services
	 */
	final static int capactiyMax = 30;
	// Création de service
	public static void createServicesMenu(){
		
		clearScreen();
		
		//affichage de la liste de services
		System.out.println("Liste de services\n");
		String[][] list = SystemControl.getServices();
		for(String[] row : list) {
			System.out.println(row[0] + " : " + row[1]);
		}
		timeSleep(900);
		
		System.out.println("\n\n\n");
		System.out.println("Création d'un service:");
		System.out.println("(Entrer = retour au menu)\n");
		System.out.println("Numero professionnel?");
		
		//Numero du professionnel
		String input_num_pro;
		input_num_pro = s.nextLine();
		User user = SystemControl.stateUserRegistration(input_num_pro);
		if(input_num_pro.length()==0) {
			mainMenu();
			return;
		}
		while(user == null) {
			System.out.println("Compte professionnel invalide\n");
			System.out.println("Numero professionnel?");
			input_num_pro = s.nextLine();
			user = SystemControl.stateUserRegistration(input_num_pro);
			if(input_num_pro.length()==0) {
				mainMenu();
				return;
			}
		}
		
		if(user instanceof Pro) {
			System.out.println("Le compte "+user+" est valide\n");
			String start_date, end_date, days, pro_number,service_code, comments, hour;
			int max_capacity;
			pro_number = user.getNumber();
			
			//Date de debut de service
			System.out.println("Date de début du service (DD-MM-YYYY)");
			start_date = s.nextLine();
			while( SystemControl.verifDate(start_date)==false) {
				if(start_date.length()==0) {
					mainMenu();
					return;
				}
				System.out.println("Date invalide");
				start_date = s.nextLine();
			}
			
			//Date de fin de service
			System.out.println("Date de fin du service (DD-MM-YYYY)");
			end_date = s.nextLine();
			while(SystemControl.verifDate(end_date)==false || SystemControl.compareDate(start_date, end_date) == false) {
				if(end_date.length()==0) {
					mainMenu();
					return;
				}
				System.out.println("Date invalide");
				end_date = s.nextLine();
			}
			
			//Heure de la seance
			System.out.println("Heure du service, format : HH:MM");
			hour = s.nextLine();
			while( SystemControl.checkGeneralData("hour", hour)==false) {
				if(hour.length()==0) {
					mainMenu();
					return;
				}
				System.out.println("Heure invalide, format : HH:MM");
				hour = s.nextLine();
			}
			
			//Recurence des seances
			System.out.println("Récurrence hebdomadaire du service (123 => Lundi, Mardi, Mercredi | 16 => Lundi, Samedi) ");
			days = s.nextLine();
			while( SystemControl.verifDays(days)==false) {
				if(days.length()==0) {
					mainMenu();
					return;
				}
				System.out.println("Entrée invalide");
				days = s.nextLine();
			}
			
			//Capacité maximale de la seance
			System.out.println("Capacité maximale (" + capactiyMax+")");
			max_capacity = choseNum(0,capactiyMax,true);
			while( SystemControl.verifCapacity(max_capacity)==false) {
				if(max_capacity==-1) {
					mainMenu();
					return;
				}
				System.out.println("Entrée invalide");
				max_capacity = choseNum(0,capactiyMax,true);
			}
			
			//Code du service
			System.out.println("Code du service");
			service_code = s.nextLine();
			while( SystemControl.verifCode(service_code)==false) {
				if(service_code.length()==0) {
					mainMenu();
					return;
				}
				System.out.println("Entrée invalide");
				service_code = s.nextLine();
			}
			
			//Prix
			System.out.println("Prix du service (max $100.0)");
			String price_string;
			double price = -1.0;
			price_string = s.nextLine();
			while( SystemControl.checkDataPrice(price)==false) {
				if(price_string.length()==0) {
					mainMenu();
					return;
				}
				try {
					price = Double.parseDouble(price_string);
					price =  Math.round(price*100.0)/100.0;
				}catch (Exception e){
					price = -1.0;
				}
				if(SystemControl.checkDataPrice(price)) break;
				System.out.println("Entrée invalide");
				price_string = s.nextLine();
			}
			
			//Commentaire
			System.out.println("Commentaires (facultatif).");
			comments = s.nextLine();
			
			//Creation du service et des seances
			Service service = SystemControl.addService(start_date, end_date, hour, days,max_capacity,pro_number, service_code, comments, price);
			if(service != null)
				System.out.println(service);
			else 
				System.out.println("Erreur lors de la création du service.");
		
		} else if(user instanceof Client) {
			
			System.out.println("Compte client, les clients ne sont pas autorisés à créer des services.");
			
		}

		returnMainMenu();
		return;
	}
	
	/**
	 * Menu pour ajouter ou modifier un service
	 */
	public static void modifServicesMenu(){
		
		clearScreen();
		System.out.println("Modification de service:");
		
		//Numero du professionnel
		System.out.println("Numéro professionnel? ");
		String  input_num_pro = s.nextLine();
		User user = SystemControl.stateUserRegistration(input_num_pro); 		
		while(user == null){
			if(input_num_pro.length()==0) {
				mainMenu();
				return;
			}
			System.out.println("Compte professionnel invalide\n");
			System.out.println("Numéro professionnel? (Entrer = retour)");
			input_num_pro = s.nextLine();
			user = SystemControl.stateUserRegistration(input_num_pro);
			
		}
	
		if(user instanceof Pro) {
			
			//code du professionnel
			input_num_pro = user.getNumber();
			
			// recuperation des seances du professionnel
			ArrayList<Seance> seancesPro = SystemControl.getListSeancePro(input_num_pro);
		
			if(seancesPro.isEmpty()) {
				System.out.println("Le professionnel ne donne aucune séance.");
			}else {
				//affichage de toute les seance delivrées.
				System.out.println("------------------------------------");
				for(Seance seance : seancesPro) {
					System.out.println(seance.toStringClient());
					System.out.println("------------------------------------");
				}
				
				//choix de la séance a modifier
				System.out.println("Numéro de séance à modifiée?");
				String input_num_seance =  s.nextLine();
				while (!SystemControl.seanceExist(input_num_seance)) {
					if(input_num_seance.length()==0) {
						mainMenu();
						return;
					}
					System.out.println("\nNuméro de séance invalide. Numéro de séance?");
					input_num_seance =  s.nextLine();
				} 
				
				clearScreen();
				
				//Affichage de la seance sélectionner
				System.out.println("Seance sélectionée:");
				System.out.println("------------------------------------------\n");
				Seance seance = SystemControl.getSeance(input_num_seance);
				System.out.println(seance.toStringClient());
				System.out.println("------------------------------------------\n");
				
				
				System.out.println("1:Modification de la Seance");
				System.out.println("2:Suppression de la Seance.");
				System.out.println("0:Retour");
				System.out.print("Choix :");
				//Choix de maintenance a realiser
				int choice = choseNum(0,2,true);
				switch(choice) {
					case 0: //Quitter
						break;
					case 1: //modification
						String date_service, comments, hour;
						
						//Modication de la date pour la seance de service
						System.out.println("Date du service (DD-MM-YYYY)");
						date_service = s.nextLine();
						while( SystemControl.verifDate(date_service)==false) {
							if(date_service.length()==0) {
								mainMenu();
								return;
							}
							System.out.println("Date invalide");
							date_service = s.nextLine();
						}
						
						//Modification de l'horaire pour la seance de service
						System.out.println("Heure du service, format : HH:MM");
						hour = s.nextLine();
						while( SystemControl.checkGeneralData("hour", hour)==false) {
							if(hour.length()==0) {
								mainMenu();
								return;
							}
							System.out.println("Heure invalide, format : HH:MM");
							hour = s.nextLine();
						}
						
						//Modifiction de la capacité maximale pour la séance
						System.out.println("Capacité maximale (" + capactiyMax+")");
						int max_capacity = choseNum(0,capactiyMax,true);
						while( SystemControl.verifCapacity(max_capacity)==false) {
							if(max_capacity==-1) {
								mainMenu();
								return;
							}
							System.out.println("Entrée invalide");
							max_capacity = choseNum(0,capactiyMax,true);
						}
						
						//Modification du prix du service 
						System.out.println("Prix du service (max $100.0)");
						String price_string;
						double price = -1.0;
						price_string = s.nextLine();
						while( SystemControl.checkDataPrice(price)==false) {
							if(price_string.length()==0) {
								mainMenu();
								return;
							}
							try {
								price = Double.parseDouble(price_string);
								price =  Math.round(price*100.0)/100.0;
							}catch (Exception e){
								price = -1.0;
							}
							if(SystemControl.checkDataPrice(price)) break;
							System.out.println("Entrée invalide");
							price_string = s.nextLine();
						}
						
						//Modification du commentaire concernant la seance
						System.out.println("Commentaires (facultatif).");
						comments = s.nextLine();
						
						//Modificaition de la seance
						if(SystemControl.editSeance(input_num_seance, input_num_pro, date_service, hour, max_capacity, price, comments)) {
							System.out.println("La modification a été réaliser avec succès");
						}else {
							System.out.println("La modification n'a pas pu être réalisée");
						}

						returnMainMenu();
						return;
						
					case 2: //suppression de seance
						//confirmation de la supression
						System.out.println("Confirmation de supression ? o/n");
						String input_supr;
						input_supr =s.nextLine();
						while(input_supr.length()==0||input_supr.length()>1 || (input_supr.charAt(0) != 'o' && input_supr.charAt(0) != 'n')) {
							System.out.println("Entre invalide");
							input_supr =s.nextLine();
						}
						
						if(input_supr.charAt(0) == 'o') { //supression
							if(SystemControl.deleteSeance(input_num_seance,input_num_pro)) {
								System.out.println("La supression a été réaliser avec succès");
							}else {
								System.out.println("La supression n'a pas pu être réalisée");
							}
						}
						//anulation
						if(input_supr.charAt(0) == 'n') { 
							System.out.println("Supression annulée");
						}
						returnMainMenu();
						return;
				}
			}
		}else {
			System.out.println("Le compte est un compte client, et ne peut donc pas modifier de service.");
		}
		returnMainMenu();
		return;
	}		
			
	/**
	 * Menu pour inscrire un membre a une séance
	 */
	public static void inscriptionService(){
		clearScreen();
		System.out.println("Inscription à un service");
		System.out.println("(Entrer = retour au menu)\n");
		System.out.println("Entrez le code d'identification");
		
		//code de membre
		String  input_code = s.nextLine();
		User user =  SystemControl.stateUserRegistration(input_code); 
		while(user == null){
			if(input_code.length()==0) {
				mainMenu();
				return;
			}
			System.out.println("Compte invalide. Numéro du membre? (Entrer = retour)");
			input_code = s.nextLine();
			user = SystemControl.stateUserRegistration(input_code);
			
		}
		input_code = user.getNumber();
		
		if(user instanceof Client) {//compte client
			if(user.getState()) {
				System.out.println("Le compte "+user+" valide");
				
				//recuperation des seances d'aujourd'hui
				ArrayList<Seance> list_seance_daily = SystemControl.getServicesDirectory();
				if(list_seance_daily.isEmpty()) {
					System.out.println("Pas de scéance aujourd'hui.");
				}else {
					//affichage des seances
					System.out.println("------------------------------------");
					for(Seance seance : list_seance_daily) {
						System.out.println(seance.toStringClient());
						System.out.println("------------------------------------");
					}
					
					//selection de la seance d'inscription
					System.out.println("\nSéance choisie?");
					String input_num_seance = s.nextLine();
					while (!SystemControl.seanceExist(input_num_seance)) {
						if(input_num_seance.length() == 0) {
							mainMenu();
							return;
						}
						System.out.println("Numéro de séance invalide. Numéro seance?");
						input_num_seance = s.nextLine();
					}
					
					//reglement des frais
					String confirmation ="";
					Seance seance = SystemControl.getSeance(input_num_seance);
					System.out.println("Les fraits de services sont de $" + seance.getPrice() );
					System.out.println("Confirmer inscription quand le clients a payer ?\nEntrer \"o\" quand le client a payé.\nEntrer \"n\" pour annuler si le client ne peut pas payer");
					while(confirmation.length()==0 || (confirmation.charAt(0) != 'o' && confirmation.charAt(0) != 'n')) {
						confirmation =s.nextLine();
					}
					if(confirmation.charAt(0) == 'o') {
						System.out.println("Commentaire (facultatif)?");
						String comments =s.nextLine();
						Registration enregistrement = SystemControl.registerClient(input_num_seance,input_code,comments);
						if(enregistrement != null){
							System.out.println("Inscription réalisée avec succès:\n");
							System.out.println(enregistrement);
						}else {
							System.out.println("Erreur lors de l'inscription.");
						}
					}
				}
			} else {//compte suspendu
				System.out.println("Le compte "+ user +" est suspendu ");
			}
		} else if(user instanceof Pro) {//compte professionnel
			System.out.println("Compte de professionnel , non autorisé à suivre une séance");
		}

		returnMainMenu();
		return;
	}
	
	
	
	/**
	 * Menu qui affiche au proffesionnel les inscrits a ses séances
	 */
	public static void consultationServicePro(){
		clearScreen();
		System.out.println("Consultation des services du professiomnnel :");
		System.out.println("(Entrer = retour au menu)\n");
		System.out.println("Numéro professionnel? ");
		
		//Code du proffesionnel
		String  input_num_pro = s.nextLine();		
		User user = SystemControl.stateUserRegistration(input_num_pro); 
		while(user == null){
			if(input_num_pro.length()==0) {
				mainMenu();
				return;
			}
			System.out.println("Compte professionnel invalide\n");
			System.out.println("Numéro professionnel? (Entrer = retour)");
			input_num_pro = s.nextLine();
			user = SystemControl.stateUserRegistration(input_num_pro);
			
		}
		input_num_pro = user.getNumber();
		
		
		if (user instanceof Pro) {
			System.out.println("Compte professionnel  "+ user +" valide\n");
			
			//Liste des seance du professionnel
			ArrayList<Seance> list_seances_pro= SystemControl.getListSeancePro(input_num_pro);
			if(list_seances_pro.isEmpty()) {
				System.out.println(user + "ne donne pas de séances");
			}else {
				//affichage des seance du professionnel
				System.out.println("Voici les séances données par " + user + ":\n");
				for(Seance seance : list_seances_pro) {
					System.out.println(seance.toString());
				}
				
				//selection de la seance a afficher les participant
				System.out.println("Numéro de séance?");
				String input_num_seance =  s.nextLine();
				while (!SystemControl.seanceExist(input_num_seance)) {
					if(input_num_seance.length()==0) {
						mainMenu();
						return;
					}
					System.out.println("Numéro de séance invalide. Numéro de séance?");
					input_num_seance =  s.nextLine();
				}
				
				//Affichage des inscrits a la seance selectionnée
				ArrayList<User> list_inscrits = SystemControl.getRegistrationsSeance(input_num_seance);
				if(list_inscrits.isEmpty()) {
					System.out.println("Pas d'inscrits.");
				}else {
					System.out.println("Liste des inscrits : ");
					for(User inscrit : list_inscrits) {
						System.out.println(inscrit);
					}
				}
			}
		}else {
			System.out.println("Le compte entré est un client, et ne peut donc consulter aucun service");
		}
		returnMainMenu();
		return;
	}
	
	
	/**
	 *Menu qui permet a un membre de confirmer sa presence à un service pour lequel il est inscrit
	 */
	public static void confirmationInscription(){
		clearScreen();
		String input_num_seance = new String();
		String comments ="";
		System.out.println("Confirmation à un service");
		System.out.println("(Entrer = retour au menu)\n");
		
		//Recuperation de la liste des seance d'aujourd'hui
		ArrayList<Seance> list_seance_daily = SystemControl.getServicesDirectory();
		if(list_seance_daily.isEmpty()) {
			System.out.println("Pas de scéance aujourd'hui.");
		}else {
			
			//affichage de la liste des seances d'aujourd'hui
			System.out.println("------------------------------------");
			for(Seance seance : list_seance_daily) {
				System.out.println(seance);
				System.out.println("------------------------------------");
			}
			
			//choix de la seance
			System.out.println("\nNuméro de séance à confirmer inscription?");
			input_num_seance = s.nextLine();
			while (!SystemControl.seanceExist(input_num_seance)) {
				if(input_num_seance.length() == 0) {
					mainMenu();
					return;
				}
				System.out.println("Numero de seance invalide. Numero de seance à confirmer inscription?");
				input_num_seance = s.nextLine();
			}
			
			//comentaire d'inscription
			System.out.println("Commentaire (facultatif)?");
			comments =s.nextLine();
			
			//code du membre
			System.out.println("Entrez le code du membre");
			String  input_code = s.nextLine();		
			User user =  SystemControl.stateUserRegistration(input_code); 
			while (user == null) {
				if(input_code.length()==0) {
					mainMenu();
					return;
				}
				System.out.println("Le compte est invalide");
				input_code = s.nextLine();
				user = SystemControl.stateUserRegistration(input_code);
			}
			
			input_code = user.getNumber();
			if(user instanceof Client) {
				if(user.getState()) {
					Confirmation confirmation = SystemControl.confirmPresence(input_num_seance,input_code,comments);
					if(confirmation != null){
						System.out.println("Validé.");
					}else {//membre non inscrit a la seance selectionnée
						System.out.println("Accès Refusé.");
					}
				} else {//membre suspendu
					System.out.println("Accès Refusé.");
				}
			} else { //compte pro
				System.out.println("Accès Refusé.");
			}
		}	
		returnMainMenu();
		return;
	}
	
	
	/**
	 *Fonction qui permet la generation du rapport comptable quand le gerant le demande
	 */
	public static void rapportComptable(){
		
		clearScreen();
		
		//creation du rapport comptable
		String path = SystemControl.rapportComptable();
		
		//affichage du path comptenant le rapport
		System.out.println("Le rapport est imprimé et se trouve dans le fichier :\n"+path);
		
		returnMainMenu();
		return;
	}
	
	/**
	 *Fonction qui execute la procedure comptable manuellement
	 */
	public static void executeProcedureComptable() {
		
		clearScreen();
		
		String path = SystemControl.procedureComptable();
		if(path.length() > 0) {
			
			//affichage du path comptenant les rapports
			System.out.println(path);
		} else 
			System.out.println("Erreur lors de l'éxecution de la procédure comptable, vérifiez que le dossier 'Rapports' est bien présent à la racine du projet, \n"
					+ "Celui-ci doit aussi contenir les dossiers 'Clients' et 'Professionnels'.");
		
		returnMainMenu();
		return;
	}
	
	
	/**
	 * Api sous forme d'un menu qui permet à rnb d'effectuer la maintenance sur les membres de #GYM
	 */
	public static void rnbAPI() {
		
		clearScreen();
		System.out.println("API RnB (Modification de l'état d'un membre)");
		System.out.println("(Entrer = retour au menu)\n");
		System.out.println("Veuillez entrer le numéro du client à maintenir : ");
		
		//code du membre a maintenir
		String input_code = s.nextLine();
		User user =  SystemControl.stateUserRegistration(input_code); 
		while(user == null){
			if(input_code.length()==0) {
				mainMenu();
				return;	
			}
			System.out.println("Compte invalide, veuillez re entrer le numéro client. (Entrer = retour)");
			input_code = s.nextLine();
			user = SystemControl.stateUserRegistration(input_code);
			
		}
		
		
		if(user instanceof Client) {//compte client
			
			String etat = "";
			
			//etat du client
			if(user.getState()) {
				System.out.println("Le compte " + user + " est valide.");
				etat = "suspendu";
			}
			else {
				System.out.println("Le compte " + user + " est suspendu.");
				etat = "valide";
			}
				
			System.out.println("1 : Passer l'état du compte à " + etat);
			System.out.println("0 : Retour");
			System.out.print("Choix :");
			
			//choix de maintenance pour le compte
			int choice = choseNum(0,1,true);
			switch(choice) {
				case 0: 
					mainMenu();
					break;
				case 1:
					((Client) user).switchState();
					System.out.println("L'état du compte à été modifié avec succès.");
					returnMainMenu();
					break;
			}
			
			clearScreen();
			
		} else {
			System.out.println("Le compte "+ user + " est un compte professionnel et son état ne peut donc être modifié.");
		}
		returnMainMenu();
		return;
	}
		
		
	
	
}
