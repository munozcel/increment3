// Ryan Azoune
// Rym Bach
// Celina Ghoraieb-Munoz
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

/**
 * Modèle d'un professionnel, extension de la classe User.
 * 
 * @author
 */
public class Pro extends User {

	private ArrayList<Service> listService = new ArrayList<Service>();

	
	/**
	 * Construction d'une instance professionnel.
	 * 
	 * @param first_name Prénom.
	 * @param last_name Nom.
	 * @param birth_date Date de naissance.
	 * @param email Adresse email.
	 * @param phone Numéro de téléphone.
	 * @param address Adresse postale.
	 * @param city Ville.
	 * @param province Province.
	 * @param postal_code Code postal.
	 */
	public Pro(String first_name, String last_name, String birth_date, String email, String phone, String address, String city, String province, String postal_code) {
		
		super(first_name, last_name, birth_date, email, phone, address, city, province, postal_code);
	
	}

	/**
	 * Construction d'une instance professionnel de démonstration.
	 * 
	 * @param first_name Prénom.
	 * @param last_name Nom.
	 * @param birth_date Date de naissance.
	 * @param email Adresse email.
	 * @param phone Numéro de téléphone.
	 * @param address Adresse postale.
	 * @param city Ville.
	 * @param province Province.
	 * @param postal_code Code postal.
	 * 
	 * @param code_demo Numéro de démonstration du professionnel.
	 */
	public Pro(String first_name, String last_name, String birth_date, String email, String phone, String address, String city, String province, String postal_code, String code_demo) {
		
		super(first_name, last_name, birth_date, email, phone, address, city, province, postal_code, code_demo);
	
	}
	
	/**
	 * Ajoute un service que le professionnel offre.
	 * 
	 * @param start_date Date de début du service.
	 * @param end_date Date de fin du service.
	 * @param hour Heure.
	 * @param days Récurrence.
	 * @param max_capacity Capacité maximale.
	 * @param service_code Code de service.
	 * @param comments Commentaire.
	 * @param price Prix.
	 * @return Service Retourne l'instance du service créé ou null si une erreur s'est produite.
	 */
	public Service addService(String start_date, String end_date, String hour, ArrayList<String> days, int max_capacity, String service_code, String comments, double price) {
		
		Service service = new Service(start_date, end_date, hour, days, max_capacity, this.getNumber(), service_code, comments, price);
		
		if(service.getSeances().size() > 0) {
			listService.add(service);
			Database.addService(service);
			return service;
		}
		
		System.out.println("Le professionnel offre déjà 100 séances pour ce service ou aucune récurrence hebdomadaire a lieu dans l'intervalle de dates entrées.");
		return null;
	}
	
	
	/**
	 * Retourne les séances à venir du professionnel
	 * 
	 * @return ArrayList<Seance> Liste des séances à venir du professionnel.
	 */
	public ArrayList<Seance> getSeances(){
		ArrayList<Seance> listSeance = new ArrayList<Seance>();
		
		for(Service service : listService) {
			ArrayList<Seance> seances = service.getSeances();
			
			for (Seance seance : seances) {
				
				if(!seance.isCanceled() && seance.getDate().plusDays(1).isAfter(LocalDate.now()))
					listSeance.add(seance);
				
			}
			
		}
		return listSeance;
	}
	
	
	/**
	 * Retourne toutes les séances du professionnel.
	 * 
	 * @return ArrayList<Seance> Liste de toutes les séances du professionnel.
	 */
	public ArrayList<Seance> getAllSeances(){
		ArrayList<Seance> listSeance = new ArrayList<Seance>();
		
		for(Service service : listService) {
			
			ArrayList<Seance> seances = service.getSeances();
			for (Seance seance : seances) {
				
				if(!seance.isCanceled())
					listSeance.add(seance);
				
			}
			
		}
		return listSeance;
	}

	
	/**
	 * Supprime le professionnel si celui-ci ne donne aucune séance dans le futur.
	 * 
	 * @return boolean true lorsque le professionnel à été supprimé ou false si la suppression n'a pas eu lieu.
	 */
	@Override
	public boolean deleteUser() {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
		LocalDate date = LocalDate.parse(dateFormat.format(d));
		
		for(Service service : this.listService) {
			ArrayList<Seance> listSeance = service.getSeances();
			
			for(Seance seance : listSeance) {
				
				if(seance.getDate().plusDays(1).isAfter(date) && !seance.isCanceled()) {
					System.out.println("Le professionnel offre des séance dans le futur.");
					return false;
				}
				
			}
			
		}
		Database.deleteUser(this.getNumber());
		return true;
	}

	
	/**
	 * Retourne l'état du professionnel.
	 * 
	 * @return boolean true Un professionnel est toujours valide.
	 */
	@Override
	public boolean getState() {
		return true;
	}
	

}
