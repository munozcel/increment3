// Ryan Azoune
// Rym Bach
// Celina Ghoraieb-Munoz
/**
 * Classe de lancement de GYM.
 *
 * @author
 */


public class Main {
    public static void main(String[] args){
        // Exécution du menu principal.
        Menu.mainMenu();

        // Arrêt du système.
        System.exit(0);
    }
}
