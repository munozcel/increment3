// Ryan Azoune
// Rym Bach
// Celina Ghoraieb-Munoz
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Modèle d'une séance.
 * 
 * @author
 */
public class Seance {
	
	private LocalDate date;
	private LocalTime hour; 
	private int max_capacity;
	private ArrayList<User> users;
	private String pro_number;
	private String service_code;
	private String comments;
	private String key;
	private double price;
	private boolean canceled;
	
	
	/**
	 * Construction d'un modèle de séance.
	 * 
	 * @param date Date de la séance.
	 * @param hour Heure.
	 * @param max_capacity Capacité maximale.
	 * @param pro_number Numéro du professionnel qui offre la séance.
	 * @param service_code Code de service de la séance.
	 * @param comments Commentaire.
	 * @param price Prix.
	 * @param key Numéro de séance.
	 */
	public Seance(LocalDate date, LocalTime hour, int max_capacity, String pro_number, String service_code, String comments, double price, String key) {
		
		this.date = date;
		this.hour = hour;
		this.max_capacity = max_capacity;
		this.users = new ArrayList<User>();
		this.pro_number = pro_number;
		this.service_code = service_code;
		this.comments = comments;
		this.price = price;
		this.canceled = false;
		this.key = key;
		
		Database.addSeance(key, this);
	}
	
	/**
	 * Inscrit un utilisateur à la séance.
	 * 
	 * @param user Utilisateur à inscrire.
	 * @return boolean Validation de l'inscription ou non
	 * @throws Exception Différents messages d'erreurs (Capacité maximale atteinte, utilisateur déjà inscrit, séance annulée).
	 */
	public boolean register(User user) throws Exception {
		if(!this.has_vacancy()) {
			throw new Exception("Capacité de la séance dépassée.");
		}
		
		if(this.users.contains(user)) {
			throw new Exception("Utilisateur déjà inscrit à cette séance.");
		}
		if(this.canceled) {
			throw new Exception("La séance à été annulée et aucune inscription n'est donc autorisée");
		}
		
		this.users.add(user);
		return true;
	}
	
	/**
	 * Retourne le nombre d'utilisateurs inscrits à la séance.
	 * 
	 * @return int Nombre d'inscrits.
	 */
	public int nb_registrations() {
		return this.users.size();
	}
	
	/**
	 * Retourne le code de service de la séance.
	 * 
	 * @return String code de service.
	 */
	public String getServiceCode() {
		return this.service_code;
	}
	
	/**
	 * Retourne le nom du service de la séance.
	 * 
	 * @return String nom du service.
	 */
	public String getServiceName() {
		return Database.getlistServices().get(this.service_code);
	}
	
	/**
	 * Retourne le service contenant la séance
	 * 
	 * @return Service Service contenant la séance.
	 */
	public Service getService() {
		for(Object service : Database.getListService().toArray()) {
			Service real_service = (Service) service;
			
			if(real_service.getSeances().contains(this)) {
				return real_service;
			}
		}
		
		return null;
	}
	
	/**
	 * Vérifie qu'il reste de la place dans la séance.
	 * 
	 * @return boolean
	 */
	public boolean has_vacancy() {
		return this.nb_registrations() < max_capacity;
	}
	
	/**
	 * Retourne la date de la séance.
	 * 
	 * @return LocalDate Date de la séance.
	 */
	public LocalDate getDate() {
		return this.date;
	}
	
	/**
	 * Retourne le numéro du professionnel qui donne la séance.
	 * 
	 * @return String numéro du professionnel.
	 */
	public String getPro() {
		return this.pro_number;
	}
	
	/**
	 * Retourne le numéro de la séance.
	 * 
	 * @return String numéro de la séance.
	 */
	public String getKey() {
		return this.key;
	}
	
	/**
	 * Retourne la liste des utilisateurs inscrits à la séance.
	 * 
	 * @return ArrayList<User> Liste des utilisateurs inscrits.
	 */
	public ArrayList<User> getRegistrations(){
		return this.users;
	}
	
	/**
	 * Vérifie si un utilisateur est inscrit à la séance.
	 * 
	 * @param user Instance de l'utilisateur.
	 * @return boolean inscrit : true et false si pas inscrit.
	 */
	public boolean containsUser(User user) {
		return users.contains(user);
	}
	
	/**
	 * Désinscrit un utilisateur de la séance.
	 * 
	 * @param user Instance de l'utilisateur à désinscrire.
	 */
	public void deleteUser(User user) {
		this.users.remove(user);
	}
	
	/**
	 * Retourne le prix de la séance.
	 * 
	 * @return double Prix.
	 */
	public double getPrice() {
		return this.price;
	}
	
	/**
	 * Vérifie si la séance est annulée.
	 * 
	 * @return boolean true si annulée, false sinon
	 */
	public boolean isCanceled() {
		return this.canceled;
	}
	
	/**
	 * Retourne la capacité maximale de la séance.
	 * 
	 * @return int Capacité maximale.
	 */
	public int getCapacity() {
		return this.max_capacity;
	}
	
	
	/**
	 * Modifie les informations de la séance.
	 * 
	 * @param pro_number Numéro du professionnel qui offre la séance.
	 * @param date Nouvelle date de la séance.
	 * @param hour Nouvelle heure de la séance.
	 * @param max_capacity Nouvelle capacité maximale de la séance.
	 * @param price Nouveau prix de la séance.
	 * @param comments Commentaire.
	 * @return boolean True si modification effectuée.
	 * @throws Exception Exception si les données entrées ne sont pas valies, ou si la séance a été annulée ou si ce n'est pas le bon professionnel qui donne la séance.
	 */
	public boolean editSeance(String pro_number, String date, String hour, int max_capacity, double price, String comments) throws Exception {
		
		if(pro_number == null || date == null || hour == null || comments == null)
			throw new Exception("Les données entrées ne sont pas valides.");
		
		if(!pro_number.equals(this.pro_number)) {
			
			throw new Exception("La séance séléctionnée n'appartient pas au bon professionnel.");
			
		} else if(this.canceled) {
			
			throw new Exception("La séance séléctionnée à été annulée.");
			
		} else {
			
			if(!SystemControl.verifDate(date))
				throw new Exception("La date n'est pas valide.");
			if(!SystemControl.checkGeneralData("hour", hour))
				throw new Exception("L'heure n'est pas valide.");
			if(!(max_capacity > 0 && max_capacity < 31))
				throw new Exception("La capacité maximale n'est pas correcte.");
			if(!SystemControl.checkDataPrice(price))
				throw new Exception("Le prix n'est pas correct.");
			if(!SystemControl.checkDataComments(comments))
				throw new Exception("Le commentaire n'est pas valide.");
			
			this.date = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
			this.hour = LocalTime.parse(hour, DateTimeFormatter.ofPattern("HH:mm"));
			this.max_capacity = max_capacity;
			this.price = price;
			this.comments = comments;
			
			return true;
		}
	}

	/**
	 * Annule la séance.
	 * 
	 * @param pro_number Numéro du professionnel qui donne la séance.
	 * @return boolean True si la séance a été annulée.
	 * @throws Exception Si le professionnel n'est pas celui qui donne la séance.
	 */
	public boolean deleteSeance(String pro_number) throws Exception {
		if(pro_number == null) throw new Exception("La séance séléctionnée n'appartient pas au bon professionnel. (Le professionnel n'existe pas.)");
		if(!pro_number.equals(this.pro_number)) {
			throw new Exception("La séance séléctionnée n'appartient pas au bon professionnel.");
		} else {
			this.canceled = true;
			return true;
		}
			
	}
	
	/**
	 * Permet l'affichage complet des informations de la séance.
	 * 
	 * @return String informations de la séances.
	 */
	public String toStringClient() {
		String seance = new String();
		seance = "Service : " + Database.getService(this.service_code) + " - " + this.service_code + "\n"
				+ "Code de la séance : " + this.key + "\n"
				+ "Date : " + this.date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "\n"
				+ "Heure : " + this.hour + "\n"
				+ "Code du professionnel : " + this.pro_number + "\n"
				+ "Capacité maximale : " + this.max_capacity + "\n"
				+ "Prix : " + this.price + "$\n"
				+ "Commentaire : " + this.comments + "\n";
		return seance;
	}
	
	/**
	 * Override de la méthode toString(), affiche un résumé des informations de la séance.
	 * 
	 */
	@Override
	public String toString() {
		String seance = new String();
		seance = "Date : " + this.date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + ", Heure: " + this.hour + ", Code de service :  " + this.service_code + ", Code de la séance : " + this.key + ", Nombre d'inscrits : " + this.nb_registrations();
		return seance;
	}
	
}
