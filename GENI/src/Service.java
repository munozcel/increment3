// Ryan Azoune
// Rym Bach
// Celina Ghoraieb-Munoz
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

/**
 * Modèle d'un service.
 * 
 * @author
 */
public class Service {
	
	private LocalDateTime creation_datetime;
	private LocalDate start_date;
	private LocalDate end_date;
	private LocalTime hour;
	private ArrayList<String> days;
	private int max_capacity;
	private String pro_number;
	private String service_code;
	private String comments;
	private double price;
	private ArrayList<Seance> listSeance = new ArrayList<Seance>();
	
	/**
	 * Construction d'un modèle service.
	 * 
	 * @param start_date Date de début du service.
	 * @param end_date Date de fin de service.
	 * @param hour Heure du service.
	 * @param days Récurrence du service.
	 * @param max_capacity Capacité maximale.
	 * @param pro_number Numéro du professionnel qui donne le service.
	 * @param service_code Numéro de service.
	 * @param comments Commentaire.
	 * @param price Prix.
	 */
	public Service(String start_date, String end_date, String hour, ArrayList<String> days, int max_capacity, String pro_number, String service_code, String comments, double price) {
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String date = dateFormat.format(new Date());
		
		this.creation_datetime = LocalDateTime.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
		this.start_date = LocalDate.parse(start_date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		this.end_date = LocalDate.parse(end_date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		this.hour = LocalTime.parse(hour, DateTimeFormatter.ofPattern("HH:mm"));
		
		this.days = days;
		if(max_capacity > 30) max_capacity = 30;
		this.max_capacity = max_capacity;
		
		this.pro_number = pro_number;
		this.service_code = service_code;
		this.comments = comments;	
		
		if(price > 100.00) price = 100.0;
		
		this.price = price;
		this.createSeances();
		
	}
	
	/**
	 * Génération des séances du service en fonction de la récurrence hebdomadaire de celui-ci.
	 */
	private void createSeances() {
		
		for (LocalDate date = this.start_date; date.isBefore(this.end_date.plusDays(1)); date = date.plusDays(1))
		{
			
			if(this.days.contains(date.getDayOfWeek().name().toUpperCase())) {
				
				String key = generateKey();
				if(key != null) {
					
					Seance seance = new Seance(date, this.hour, this.max_capacity, this.pro_number, this.service_code, this.comments, this.price, key);
					this.listSeance.add(seance);
					
				}
				else {
					
					this.end_date = this.listSeance.get(this.listSeance.size()-1).getDate();
					break;
					
				}
			}
		}
	}
	
	/**
	 * Génération des clés des séances du service. 
	 * 
	 * @return String clé de séance.
	 */
	private String generateKey() {
		
		String code = this.service_code;
		String lastNumbers = this.pro_number.substring(this.pro_number.length()-2);
		String key = code + "00" + lastNumbers;
		int i = 0;
		
		while(i < 100 && Database.containKeySeance(key)) {
			
			if(i < 10) 
				key = code + "0" + i + lastNumbers;
			else 
				key = code + i + lastNumbers;
			
			i++;
		}
		if (i < 100) 
			return key;
		
		return null;
	}
	
	
	/**
	 * Retourne la séance de la journée actuelle.
	 * 
	 * @return Seance Instance de la séance.
	 */
	public Seance getDailySeance() {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
		LocalDate date = LocalDate.parse(dateFormat.format(d));
		
		for(Seance seance : this.listSeance) {
			
			if(seance.getDate().equals(date) && !seance.isCanceled()) 
				return seance;
		}
		return null;
	}
	
	/**
	 * Retourne la liste des séances du service.
	 * 
	 * @return ArrayList<Seance> List des seances.
	 */
	public ArrayList<Seance> getSeances(){
		return this.listSeance;
	}
	
	/**
	 * Retourne la date de création du service.
	 * 
	 * @return String date de création.
	 */
	public String getCreationDateTime() {
		return creation_datetime.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
	}
	
	/**
	 * Retourne le code de service qui est offert.
	 * 
	 * @return String code de service.
	 */
	public String getServiceCode() {
		return this.service_code;
	}

	/**
	 * Override de la méthode toString pour afficher proprement toutes les informations utiles du service.
	 * 
	 */
	@Override
	public String toString() {
		
		String info = new String();
		
		info += "Date et heure de la création du service : " + getCreationDateTime() + "\n";
		info += "Date de début du service : " + this.start_date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "\n";
		info += "Date de fin du service : " + this.end_date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "\n";
		info += "Heure de service : " + this.hour + "\n";
		info += "Récurrence hebdomadaire du service : ";
		
		for(String d : this.days) {
			switch(d) {
				case "MONDAY":
					d = "Lundi";
					break;
				case "TUESDAY":
					d = "Mardi";
					break;
				case "WEDNESDAY":
					d = "Mercredi";
					break;
				case "THURSDAY":
					d = "Jeudi";
					break;
				case "FRIDAY":
					d = "Vendredi";
					break;
				case "SATURDAY":
					d = "Samedi";
					break;
				case "SUNDAY":
					d = "Dimanche";
					break;
			}
			info += d + " ";
		}
		info += "\nCapacité maximale : " + this.max_capacity + "\n";
		info += "Numéro du professionnel : " + this.pro_number + "\n";
		info += "Code de service : " + this.service_code + "\n";
		info += "Prix : " + this.price + "$\n";
		
		if (this.comments.length() > 0) 
			info += "Commentaire : " + this.comments;
		
		return info;
	}
	
}
