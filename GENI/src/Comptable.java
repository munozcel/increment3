// Ryan Azoune
// Rym Bach
// Celina Ghoraieb-Munoz
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Procédures comptables (fichiers TEF et rapports).
 * 
 * @author
 */
public class Comptable {

	/**
	 * Calcule la date du dernier vendredi à compter d'une date donnée.
	 * 
	 * @param d Date de départ donnée.
	 * @return LocalDate Date du dernier vendredi avant la date donnée.
	 */
	private static LocalDate calcLastFriday(LocalDate d) {
		  return d.with(TemporalAdjusters.previous((DayOfWeek.FRIDAY)));
	}
	
	/**
	 * Retourne le numéro de la semaine dans l'année.
	 * 
	 * @return Numéro de la semaine dans l'année.
	 */
	private static int getWeek() {
		LocalDate date = getDate();
		WeekFields weekFields = WeekFields.of(Locale.getDefault()); 
		
		int week = date.get(weekFields.weekOfWeekBasedYear());
		
		return week;
	}
	
	/**
	 * Retourne la date actuelle au format "AAAA-MM-JJ".
	 * 
	 * @return LocalDate Date actuelle au format "AAAA-MM-JJ".
	 */
	private static LocalDate getDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
		
		LocalDate date = LocalDate.parse(dateFormat.format(d));
		
		return date;
	}
	
	/**
	 * Procédure comptable destinée à être exécutée tous les vendredis soir.
	 * 
	 * @return String Message de réussite, si tout a bien été exécuté.
	 * @throws IOException Erreur d'entrée-sortie si le dossier est introuvable.
	 */
	public static String procedure() throws IOException {
		int week = getWeek();
		
		File dir = new File("TEF-WEEK-" + week);
		
		dir.mkdirs();
		
		HashMap<String, NavigableMap<Long, String>> user_report = new HashMap<String, NavigableMap<Long, String>>();
		
		for(Map.Entry<String, User> entry : Database.getListUser().entrySet()) {
			User user = entry.getValue();
			
			if(user instanceof Pro) {
				user = (Pro) user;
				String name = user.getFirstName() + " " + user.getLastName();
				
				double price = 0;
				int nb_seances = 0;
				int nb_membres = 0;
				
				String name_and_number = "Nom : " + name + "\r\n"
				           			   + "Numéro du professionnel : " + user.getNumber() + "\r\n";
				
				String rapport_pro = name_and_number
								   + "Adresse du professionnel : " + user.getAddress() + "\r\n"
								   + "Ville du professionnel : " + user.getCity() + "\r\n"
								   + "Province du professionnel : " + user.getProvince() + "\r\n"
								   + "Code postal du professionnel : " + user.getPostalCode() + "\r\n"
								   + "\r\n";
				
				NavigableMap<Long, String> ordered_entries = new TreeMap<Long, String>();
				
				for(Seance seance : ((Pro) user).getAllSeances()) {
					LocalDate seanceDate = seance.getDate();
					
					if(seanceDate.isAfter(calcLastFriday(getDate())) && seanceDate.isBefore(getDate().plusDays(1)) && !seance.isCanceled()) {
						price += seance.getPrice() * seance.getRegistrations().size();
						nb_membres += seance.getRegistrations().size();
						
						nb_seances++;
												
						for(User registered_user : seance.getRegistrations()) {
							long mili = seance.getDate().atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
							
							// On peut avoir jusqu'à un million de conflits entre deux dates, car
							// les LocalDate finissent toutes par "00000".
							while(ordered_entries.get(mili) != null) {
								mili++;
							}
							
							String pro = "[Service rendu]\r\n"
									   + "Date du service : " + seance.getDate() + "\r\n"
								       + "Date et heure de la réception des données : " + seance.getService().getCreationDateTime() + "\r\n"
								       + "Nom du membre : " + registered_user.getFirstName() + " " + registered_user.getLastName() + "\r\n"
								       + "Numéro du membre : " + registered_user.getNumber() + "\r\n"
								       + "Code de la séance : " + seance.getKey() + "\r\n" 
								       + "Montant à payer : " + seance.getPrice() + "\r\n"
								       + "\r\n";
							
							ordered_entries.put(mili, pro);
							
							String registered_user_name = registered_user.getFirstName() + "_" + registered_user.getLastName();
							
							String client = "[Service reçu]\r\n"
									      + "Date du service : " + seance.getDate() + "\r\n"
								          + "Nom du professionnel : " + user.getFirstName() + " " + user.getLastName() + "\r\n"
								          + "Nom du service : " + seance.getServiceName() + "\r\n"
								          + "\r\n";
							
							if(user_report.get(registered_user_name) == null) {
								// Client n'ayant pas encore de rapport, nous lui créons un entête.
								NavigableMap<Long, String> ordered_user_entries = new TreeMap<Long, String>();
								
								String header = "Nom du membre : " + registered_user.getFirstName() + " " + registered_user.getLastName() + "\r\n"
									          + "Numéro du membre : " + registered_user.getNumber() + "\r\n"
							        		  + "Adresse du professionnel : " + registered_user.getAddress() + "\r\n"
											  + "Ville du professionnel : " + registered_user.getCity() + "\r\n"
											  + "Province du professionnel : " + registered_user.getProvince() + "\r\n"
										      + "Code postal du professionnel : " + registered_user.getPostalCode() + "\r\n"
											  + "\r\n";
								
								ordered_user_entries.put((long) 0 /* En début complet du rapport */, header);
								
								user_report.put(registered_user_name, ordered_user_entries);
							}
														
							NavigableMap<Long, String> ordered_user_entries = user_report.get(registered_user_name);
							
							long mili_user = seance.getDate().atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
							
							// On peut avoir jusqu'à un million de conflits entre deux dates, car
							// les LocalDate finissent toutes par "00000".
							while(ordered_user_entries.get(mili_user) != null) {
								mili_user++;
							}
							
							ordered_user_entries.put(mili_user, client);
							
							user_report.put(registered_user_name, ordered_user_entries);
						}
					}
				}
				
				for(Long key : ordered_entries.keySet()) {
				    rapport_pro += ordered_entries.get(key);
				}
				
				rapport_pro += "---------\r\n"
						     + "Membres rencontrés : " + nb_membres + "\r\n"
						     + "Nombre de séances données : " + nb_seances + "\r\n"
						     + "Total des frais : " + price;
				
				String fileName = dir + "/Week-" + week + "-Pro-" + user.getNumber() + ".tef";
				PrintWriter writer = new PrintWriter(fileName, "UTF-8");
				
				String content = name_and_number + "Montant à transférer : " + price;
				
				writer.print(content);
				writer.close();
				
				String proReportFileName = "Rapports/Professionnels/" + user.getFirstName() + "_" + user.getLastName() + "_" + getDate() + ".txt";
				PrintWriter proReportWriter = new PrintWriter(proReportFileName, "UTF-8");
				
				proReportWriter.print(rapport_pro);
				proReportWriter.close();
			}
		}
		
		for(Map.Entry<String, NavigableMap<Long, String>> entry : user_report.entrySet()) {
			String clientReportFileName = "Rapports/Clients/" + entry.getKey() + "_" + getDate() + ".txt";
			PrintWriter clientReportWriter = new PrintWriter(clientReportFileName, "UTF-8");
			
			String text = "";
			
			for(Long key : entry.getValue().keySet()) {
				text += entry.getValue().get(key);
			}
			
			clientReportWriter.print(text);
			clientReportWriter.close();
		}
		
		String path = rapport();
		
		return "La procédure comptable s'est déroulée avec succès, vous trouvez les fichiers TEF dans le dossier : " + dir.toString() + 
				"\r\nVous trouverez le rapport gérant dans le fichier : " + path + "\r\n\r\n" +
				"Les rapports par professionnels et par clients sont respectivement dans \"Rapports/Professionnels\" et \"Rapports/Clients\".\r\n" +
				"Ceux générés aujourd'hui comprennent la date d'aujourd'hui (" + getDate() + ") dans le nom du fichier.";
	}
	
	/**
	 * Crée un rapport comptable à la demande du gérant de #GYM.
	 * 
	 * @return String Nom du fichier de rapport généré.
	 * @throws IOException Erreur d'entrée-sortie si le dossier est introuvable.
	 */
	public static String rapport() throws IOException {
		int nbPro = 0;
		int nbService = 0;
		int week = getWeek();
		
		double totalPrice = 0;

		String fileName = "Rapports/Rapport-" + week +".txt";
		PrintWriter writer = new PrintWriter(fileName, "UTF-8");
		
		for(Map.Entry<String, User> entry : Database.getListUser().entrySet()) {
			User user = entry.getValue();
			
			if(user instanceof Pro) {
				user = (Pro) user;
				String name = user.getFirstName() + " " +user.getLastName();
				
				double pricePro = 0;
				int nbServicePro = 0;
				
				for(Seance seance : ((Pro) user).getAllSeances()) {
					LocalDate seanceDate = seance.getDate();
					
					if(seanceDate.isAfter(calcLastFriday(getDate())) && seanceDate.isBefore(getDate().plusDays(1))) {
						totalPrice += seance.getPrice() * seance.getRegistrations().size();
						pricePro += seance.getPrice() * seance.getRegistrations().size();
						
						nbService++;
						nbServicePro++;
					}
				}
				
				if(nbServicePro > 0) {
					nbPro++;
				}
				
				writer.println(name);
				writer.println("Nombre de services offerts : " + nbServicePro);
				writer.println("Total de frais : " + pricePro);
			}
		}
		writer.println("\r\n---------");
		writer.println("Nombre total de services offerts : " + nbService);
		writer.println("Nombre total de professionnels ayant offert des services : " + nbPro);
		writer.print("Total des frais : " + totalPrice);
		
		writer.close();
		
		return "" + fileName;
	}
}
