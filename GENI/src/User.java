// Ryan Azoune
// Rym Bach
// Celina Ghoraieb-Munoz
/**
 * Classe utilisateur (professionnels/ utilisateurs).
 * 
 * @author
 */
public abstract class User {
	private String first_name;
	private String last_name;
	private String birth_date;
	private String unique_number;
	private String email;
	private String phone;
	private String address;
	private String city;
	private String province;
	private String postal_code;
	
	/**
	 * Construction d'un modèle utilisateur.
	 * 
	 * @param first_name Prénom.
	 * @param last_name Nom de famille.
	 * @param birth_date Date de naissance.
	 * @param email Adresse courriel.
	 * @param phone Numéro de téléphone.
	 * @param address Adresse postale.
	 * @param city Ville.
	 * @param province Province.
	 * @param postal_code Code postal.
	 */
	public User(String first_name, String last_name, String birth_date, String email, String phone, String address, String city, String province, String postal_code) {
		this.first_name = first_name;
		this.last_name = last_name;
		this.birth_date = birth_date;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.city = city;
		this.province = province;
		this.postal_code = postal_code;
		
		/*
		 *  Permet d'assigner un numéro unique.
		 */
		String code = this.generateCode();
		
		while(Database.containCode(code)) {
			code = this.generateCode();
		}
		
		this.unique_number = code;
		
		Database.addUser(code, this);
	}
	
	/**
	 * Construction d'un modèle utilisateur de démonstration.
	 * 
	 * @param first_name Prénom.
	 * @param last_name Nom de famille.
	 * @param birth_date Date de naissance.
	 * @param email Adresse courriel.
	 * @param phone Numéro de téléphone.
	 * @param address Adresse postale.
	 * @param city Ville.
	 * @param province Province.
	 * @param postal_code Code postal.
	 * 
	 * @param codeDemo Code de démonstration.
	 */
	public User(String first_name, String last_name, String birth_date, String email, String phone, String address, String city, String province, String postal_code, String code_demo) {
		this.first_name = first_name;
		this.last_name = last_name;
		this.birth_date = birth_date;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.city = city;
		this.province = province;
		this.postal_code = postal_code;
		
		this.unique_number = code_demo;
		
		Database.addUser(code_demo, this);
	}
	
	/**
	 * Génération d'un code alératoire de neuf chiffres.
	 * 
	 * @return String correspond au code aléatoire de neuf chiffres.
	 */
	private String generateCode() {
		String code = new String();

		for(int i = 0; i < 9; i++) {
			code += Integer.toString((int) (Math.random() * 9));
		}

		return code;
	}
	
	/**
	 * Retourne le numéro unique d'utilisateur.
	 * 
	 * @return String Numéro unique d'utilisateur.
	 */
	public String getNumber() {
		return this.unique_number;
	}
	
	/**
	 * Retourne l'adresse courriel de l'utilisateur.
	 * 
	 * @return String Adresse courriel de l'utilisateur.
	 */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * Retourne le numéro de téléphone de l'utilisateur.
	 * 
	 * @return String Numéro de téléphone de l'utilisateur.
	 */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * Retourne la date de naissance de l'utilisateur.
	 * 
	 * @return String Date de naissance de l'utilisateur.
	 */
	public String getBirthDate() {
		return this.birth_date;
	}
	
	/**
	 * Retourne le prénom de l'utilisateur.
	 * 
	 * @return String Prénom de l'utilisateur.
	 */
	public String getFirstName() {
		return this.first_name;
	}
	
	/**
	 * Retourne le nom de famille de l'utilisateur.
	 * 
	 * @return String Nom de famille de l'utilisateur.
	 */
	public String getLastName() {
		return this.last_name;
	}
	
	/**
	 * Retourne l'adresse de l'utilisateur.
	 * 
	 * @return String Adresse de l'utilisateur.
	 */
	public String getAddress() {
		return this.address;
	}
	
	/**
	 * Retourne la ville de l'utilisateur.
	 * 
	 * @return String Ville de l'utilisateur.
	 */
	public String getCity() {
		return this.city;
	}
	
	/**
	 * Retourne la province de l'utilisateur.
	 * 
	 * @return String Province de l'utilisateur.
	 */
	public String getProvince() {
		return this.province;
	}
	
	/**
	 * Retourne le code postal de l'utilisateur.
	 * 
	 * @return String Code postal de l'utilisateur.
	 */
	public String getPostalCode() {
		return this.postal_code;
	}
	
	/**
	 * Représentation textuelle d'un utilisateur.
	 * 
	 * @return String Représentation textuelle d'un utilisateur.
	 */
	public String toString() {
		return this.first_name + " " + this.last_name + " (#" + getNumber() + ")";
	}
	
	/**
	 * Représentation textuelle des comptes de démonstration.
	 * 
	 * @return String Représentation textuelle des comptes de démonstration.
	 */
	public String toStringDemo() {
		return this.unique_number + ":" +this.first_name + " " + this.last_name + " (" + this.getClass() + ")";
	}
	
	/**
	 * Modification d'un utilisateur (adresse courriel, numéro de téléphone et informations liées à l'adresse).
	 * 
	 * @param email Nouvelle adresse courriel.
	 * @param phone Nouveau numéro de téléphone.
	 * @param address Nouvelle adresse postale.
	 * @param city Nouvelle ville.
	 * @param province Nouvelle province.
	 * @param postal_code Nouveau code postal.
	 */
	public void editUser(String email, String phone, String address, String city, String province, String postal_code) {
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.city = city;
		this.province = province;
		this.postal_code = postal_code;
	}
	
	abstract public boolean deleteUser();
	abstract public boolean getState();
}
