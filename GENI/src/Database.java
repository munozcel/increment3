// Ryan Azoune
// Rym Bach
// Celina Ghoraieb-Munoz
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Ensemble de liste de données statiques utilisé comme base de données pour le logiciel.
 * 
 * @author
 */
public class Database {

	/* Liste des données du Système */
	private static HashMap<String, User> listUser = new HashMap<String, User>();
	
	private static HashMap<String, String> listServices = new HashMap<String, String>();

	private static ArrayList<Service> listService = new ArrayList<Service>();
	
	private static HashMap<String, Seance> listSeances = new HashMap<String, Seance>();
	/* Fin de la liste de données */
	
	
	/* Ajout des données pour la démonstration.*/
	static {
		listServices.put("101", "Soccer");
		listServices.put("102", "Basket");
		listServices.put("103", "Volley");
		listServices.put("104", "Hockey");
		
		Pro pro1 = new Pro("Pro", "Test", "09-10-1997", "pro@gmail.com", "514-000-4567", "123 rue Pro", "Montréal", "Québec", "H0H 0H0", "111111111");
		
		Client client1 = new Client("NormalUn", "Test", "09-10-1997", "normalun@gmail.com", "514-123-4567", "123 rue Test", "Montréal", "Québec", "H0H 0H0", "222222222");
		Client client2 = new Client("NormalDeux", "Test", "09-10-1997", "normaldeux@gmail.com", "514-456-4567", "456 rue Test", "Montréal", "Québec", "H0H 0H0", "333333333");
		Client client3 = new Client("Suspendu", "Test", "09-10-1997", "jack@gmail.com", "514-789-4567", "789 rue Test", "Montréal", "Québec", "H0H 0H0", "444444444");
		
		client3.suspendClient();
		
		pro1.addService("01-07-2020", "31-08-2020", "18:30", SystemControl.buildDays("1234567"), 30, "101", "Ceci est un service de demo.", 55.90);
		pro1.addService("01-07-2020", "31-08-2020", "15:00", SystemControl.buildDays("135"), 30, "102", "Ceci est un service de demo LU/MA/VE.", 99.00);
		pro1.addService("01-07-2020", "31-08-2020", "08:00", SystemControl.buildDays("67"), 30, "104", "Ceci est un service de demo SA/DI.", 25.32);
		
		for(String key : listSeances.keySet() ) {
			client1.registerToSeance(key, "Inscription de démonstration.");
			client2.registerToSeance(key, "Inscription de démonstration.");
		}
	}
	/* Fin de l'ajout des données de démo. */

	/**
	 * Retourne la liste des utilisateurs.
	 * 
	 * @return HashMap<String, User>
	 */
	public static HashMap<String, User> getListUser() {
		return listUser;
	}

	/**
	 * Ajoute un utilisateur à la base de données.
	 * 
	 * @param code Numéro de l'utilisateur.
	 * @param user Instance de l'utilisateur.
	 */
	public static void addUser(String code, User user) {
		listUser.put(code, user);
	}
	
	/**
	 * Vérifie si le code utilisateur est présent dans la base de données.
	 * 
	 * @param key Numéro de l'utilisateur.
	 * @return boolean True si contenu et False sinon.
	 */
	public static boolean containCode(String key) {
		return listUser.containsKey(key);
	}
	
	/**
	 * Retourne un utilisateur.
	 * 
	 * @param key Numéro de l'utilisateur.
	 * @return User
	 */
	public static User getUser(String key) {
		return listUser.get(key);
	}
	
	/**
	 * Supprime un utilisateur de la base de données.
	 * 
	 * @param key Numéro de l'utilisateur.
	 */
	public static void deleteUser(String key) {
		listUser.remove(key);
	}
	
	/**
	 * Retourne la liste des services.
	 * 
	 * @return HashMap<String, String>
	 */
	public static HashMap<String, String> getlistServices(){
		return listServices;
	}

	/**
	 * Vérifie si un service existe.
	 * 
	 * @param code Code du service.
	 * @return boolean
	 */
	public static boolean serviceExist(String code) {
		return listServices.containsKey(code);
	}
	
	/**
	 * Retourne le nom d'un service
	 * 
	 * @param code Code de service.
	 * @return String Nom du service.
	 */
	public static String getService(String code) {
		return listServices.get(code);
	}

	/**
	 * Retourne la liste des services offerts par les professionnels.
	 * 
	 * @return ArrayList<Service> Liste des services des professionnels.
	 */
	public static ArrayList<Service> getListService() {
		return listService;
	}

	/**
	 * Ajoute un service d'un professionnel à la base de données.
	 * 
	 * @param service Instance du service
	 */
	public static void addService(Service service) {
		listService.add(service);
	}
	
	/**
	 * Retourne une séance.
	 * 
	 * @param key Numéro de séance.
	 * @return Seance Séance correspondante au numéro.
	 */
	public static Seance getSeance(String key) {
		return listSeances.get(key);
	}
	
	
	/**
	 * Vérifie que le numéro de séance existe dans la base de données.
	 * 
	 * @param key Numéro de séance.
	 * @return boolean
	 */
	public static boolean containKeySeance(String key) {
		return listSeances.containsKey(key);
	}
	
	/**
	 * Ajoute une séance à la base de données.
	 * 
	 * @param key Numéro de la séance.
	 * @param seance Instance de la séance.
	 */
	public static void addSeance(String key, Seance seance) {
		listSeances.put(key, seance);
	}
	
}
