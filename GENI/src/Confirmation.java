// Ryan Azoune
// Rym Bach
// Celina Ghoraieb-Munoz
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Modèle d'une Confirmation.
 * 
 * @author
 */

public class Confirmation {

	private String date;
	private String pro_number;
	private String client_number;
	private String service_code;
	private String comments;

	/**
	 * Construction d'un modèle de confirmation.
	 * 
	 * @param pro_number Numéro du professionnel qui donne la séance.
	 * @param client_number Numéro du client qui confirme son inscription à la séance.
	 * @param service_code Numéro de service de la séance.
	 * @param comments Commentaire de la confirmation d'inscription.
	 */
	public Confirmation(String pro_number, String client_number, String service_code, String comments) {
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = new Date();
		this.date = dateFormat.format(date);
		this.pro_number = pro_number;
		this.client_number = client_number;
		this.service_code = service_code;
		this.comments = comments;
		
	}
	
	
	/**
	 * Override de la méthode toString(), permet d'afficher correctement l'information de la confirmation.
	 * 
	 * @return String
	 */
	@Override
	public String toString() {
		
		String s = new String();
		
		s = "Date et heure actuelles :" + this.date + "\n"
				+ "Numéro du professionnel : " + this.pro_number + "\n"
				+ "Numéro du membre : " + this.client_number + "\n"
				+ "Code du service : " + this.service_code + "\n"
				+ "Commentaire : " + this.comments;
		
		return s;
	}

}
