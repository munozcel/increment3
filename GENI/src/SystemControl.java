// Ryan Azoune
// Rym Bach
// Celina Ghoraieb-Munoz
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Ensemble de fonctions statiques systèmes.
 * 
 * @author
 */
public class SystemControl {
	
	private static final Map<String, String> regex;
	static {
		
		regex = new HashMap<String, String>();
		regex.put("first_name", "[a-zA-Z]{1,20}");
		regex.put("last_name", "[a-zA-Z]{1,25}|[[a-zA-Z]+[-]*[a-zA-Z]]{1,25}");
		regex.put("email", "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		regex.put("phone", "^[2-9]\\d{2}-\\d{3}-\\d{4}$");
		regex.put("address", "[[0-9][a-zA-Z][\\s-]]{1,25}");
		regex.put("city", "[a-zA-Z]{1,14}|[[a-zA-Z]+[- \\s]*[a-zA-Z]]{1,14}");
		regex.put("province", "[a-zA-Z]{2}");
		regex.put("postal_code","[0-9a-zA-Z]{6}");
		regex.put("hour", "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$");
		
	}
	
	
	/**
	 * Fonction qui verifie a l'aide de RegEx si une entrée est valide.
	 * 
	 * @param key Type de donné a verifier.
	 * @param data La donnée a verifier.
	 * @return true si la donnée est valide false sinon
	 */
	public static boolean checkGeneralData(String key, String data) {
		
		if(regex.containsKey(key)) {
			return data.matches(regex.get(key));
		}
		
		return false;
	}

	
	/**
	 * Fonction qui verifie si une date de naissance est valide et si l'age de l'utilisateur est valide (supperieur a 5 ans)
	 * 
	 * @param birth_date La date de naissance
	 * @return true si la date de naissance est valide/recevable false sinon
	 */
	public static boolean checkDataBirthDate(String birth_date) {
		
		String regexDay = "^(0?[1-9]|[12][0-9]|3[01])\\-(0?[1-9]|1[012])\\-\\d{4}$";
		
		if(birth_date.matches(regexDay)) {
			LocalDate date = LocalDate.parse(birth_date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
			
			if(date.isBefore(LocalDate.now().minusYears(5))) {
				return true;
			}	
			
		}
		return false;
	}

	
	/**
	 * Verification du prix entrer
	 * 
	 * @param price le prix a tester
	 * @return true si le prix est valide, false sinon
	 */
	public static boolean checkDataPrice(double price) {
		return (price >= 0 && price <= 100);
	}
	
	/**
	 * Verification de la longeur du  commentaire
	 * 
	 * @param comments le commentaire a verifier
	 * @return true si le commentaire est valide, false sinon
	 */
	public static boolean checkDataComments(String comments) {
		return (comments.length() < 101);
	}
	
	
	/**
	 * Fonction qui permet la creation d'un utilisateur
	 * 
	 * @param first_name Le prenom de l'utilisateur
	 * @param last_name Le nom de l'utilisateur
	 * @param birth_date La date de naissance de l'utilisateur
	 * @param email L'email de l'utilisateur
	 * @param phone Le numéro de télephone de l'utilisateur
	 * @param address L'adresse de l'utilisateur
	 * @param city La ville de l'utilisateur
	 * @param province La province de l'utilisateur
	 * @param postal_code Le code postale de l'utilisateur
	 * @param pro Le type d'utilisateur (Pro vs client)
	 * @return Le numéro du membre.
	 */
	public static String createAccount(String first_name, String last_name, String birth_date, String email, String phone, String address, String city, String province, String postal_code, boolean pro) {
		
		for(User user : Database.getListUser().values()) {
			if((user.getEmail().toLowerCase()).equals(email.toLowerCase())) {
				return null;
			}
		}
		
		User user;
		
		if(pro) {
			user = new Pro(first_name, last_name, birth_date, email, phone, address, city, province, postal_code);
		} else {
			user = new Client(first_name, last_name, birth_date, email, phone, address, city, province, postal_code);
		}
		
		return user.getNumber();
	}
	
	

	/**
	 * Fonction qui verifie si un utilisateur fait bien partie de #GYM
	 * 
	 * @param code Code ou email de l'utilisateur
	 * @return User : l'utilisateur correspondant au code/email entré
	 */
	public static User stateUserRegistration(String code) {
		for(String key : Database.getListUser().keySet()) {
			User user = Database.getUser(key);

			if((user.getEmail().toLowerCase()).equals(code.toLowerCase())) {
				return user;
			}
		}
		
		return null;
	}
	
	/**
	 * Verification de l'existence d'une séance
	 * 
	 * @param key code de la seance 
	 * @return true si la seance existe, false sinon
	 */
	public static boolean seanceExist(String key) {
		return Database.containKeySeance(key);
	}

	/**
	 * Vérification que le code de service correspond à une service.
	 * 
	 * @param code code de service
	 * @return true si le code correspond a un service existant, false sinon
	 */
	public static boolean verifCode(String code) {
		return Database.serviceExist(code);
	}
	

	/**
	 * Fonction qui verifie la validite du format d'une date et si celle-ci est dans le future
	 * 
	 * @param date la date a verifier
	 * @return true si la date est valide, false sinon
	 */
	public static boolean verifDate(String date) {
		// DD-MM-YYYY
		String regex = "^(0?[1-9]|[12][0-9]|3[01])\\-(0?[1-9]|1[012])\\-\\d{4}$";
		if(date.matches(regex)) {
			LocalDate d = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy")).plusDays(1);
			if(d.isAfter(LocalDate.now())) {
				return true;
			}	
		}
		return false;
	}
	
	/**
	 * fonction qui compare deux dates pour savoir si la date start_date est avant end_date
	 * 
	 * @param start_date la première date
	 * @param end_date la deuxieme date
	 * @return true si la première date est avant la deuxieme
	 */
	public static boolean compareDate(String start_date, String end_date) {
		LocalDate date1 = LocalDate.parse(start_date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		LocalDate date2 = LocalDate.parse(end_date, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		date2 = date2.plusDays(1);
		return date1.isBefore(date2);
	}

	
	/**
	 * Verification du format des jours de recurences.
	 * 
	 * @param days les jours de recurence sous forme de chiffre (1=Lundi... 7=Dimanche)
	 * @return true si le format est valide, false sinon
	 */
	public static boolean verifDays(String days) {
		String regex = "^1?2?3?4?5?6?7?$";
		if(days.matches(regex) && days.length()>0) return true;
		return false;
	}
	
	/**
	 * Verification de la capacité maximale.
	 * 
	 * @param max_capacity
	 * @return true si la capacité est valide, false sinon
	 */
	public static boolean verifCapacity(int max_capacity) {
		return max_capacity > 0;
	}
	
	
	/**
	 * Fonction qui va faire la correspondance numeros entrer jours
	 * 
	 * @param days String correspondant aux jours de recurence sous forme de chiffres
	 * @return la liste des jours de recurence
	 */
	public static ArrayList<String> buildDays(String days){
		ArrayList<String> list = new ArrayList<String>();
		for(int i = 0; i < days.length(); i++) {
			if(days.charAt(i) == '1') list.add("MONDAY");
			else if(days.charAt(i) == '2') list.add("TUESDAY");
			else if(days.charAt(i) == '3') list.add("WEDNESDAY");
			else if(days.charAt(i) == '4') list.add("THURSDAY");
			else if(days.charAt(i) == '5') list.add("FRIDAY");
			else if(days.charAt(i) == '6') list.add("SATURDAY");
			else if(days.charAt(i) == '7') list.add("SUNDAY");
		}
		return list;
	}
	

	/**
	 * Fonction qui permet d'acceder a la liste de tout les services que #GYM delivre
	 * 
	 * @return la liste de tout les services que #GYM delivre
	 */
	public static String[][] getServices() {
		HashMap<String, String> list = Database.getlistServices();
		String[][] listServices = new String[list.size()][2];
		int i = 0;
		for(Map.Entry<String, String> entry : list.entrySet()) {
			listServices[i][0] = entry.getKey();
			listServices[i][1] = entry.getValue();
			i++;
		}
		return listServices;
	}
	

	/**
	 * Fonction qui va ajouter un service dans la base de données.
	 * 
	 * @param start_date date de debut de service
	 * @param end_date date de fin de service
	 * @param hour heures où le service sera delivrer
	 * @param daysInt les jours ou le service sera donner sous forme de chifre
	 * @param max_capacity capacite d'acceuil
	 * @param pro_number numero du professionnel qui delivre le service
	 * @param service_code code de service
	 * @param comments commentaire
	 * @param price frais de service
	 * @return Service instance du service qui a été, null si le service n'a pas pu être créé
	 */
	public static Service addService(String start_date, String end_date, String hour, String daysInt, int max_capacity, String pro_number, String service_code, String comments, double price) {
		try {
			ArrayList<String> days = SystemControl.buildDays(daysInt);
			Pro pro = (Pro) Database.getUser(pro_number);
			return pro.addService(start_date, end_date, hour, days, max_capacity, service_code, comments, price);
		} catch (Exception e) {
			//System.out.println("Ereur de creation de service.");
		}
		return null;
	}
	
	
	/**
	 * Fonction qui permet d'obtenir la liste de séances que fournit un professionnel
	 * 
	 * @param pro_number numero du professionnel
	 * @return La liste des seance que le professionnel delivre, null si le professionnel est introuvable
	 */
	public static ArrayList<Seance> getListSeancePro(String pro_number){
		try {
			Pro pro = (Pro) Database.getUser(pro_number);
			return pro.getSeances();
		} catch (Exception e) {
			System.out.println("Le professionnel est introuvable");
		}
		return null;
	}
	
	
	/**
	 * Fonction qui permet d'obtnier la liste des inscrits pour une séance.
	 * 
	 * @param seance_key le numero de la seance
	 * @return la liste des inscrits, null si la seance est introuvable
	 */
	public static ArrayList<User> getRegistrationsSeance(String seance_key){
		try {
			Seance seance = Database.getSeance(seance_key);
			return seance.getRegistrations();
		} catch (Exception e) {
			System.out.println("La séance est introuvable");
		}
		return null;
	}
	

	/**
	 * Fonction qui permet d'acceder au repertoir des seance d'aujourd'hui.
	 * 
	 * @return retourne la liste des séances disponible pour aujourd'hui sous forme d'une ArrayList de Seance.
	 */
	public static ArrayList<Seance> getServicesDirectory() {
		ArrayList<Service> listService = Database.getListService();
		ArrayList<Seance> listDailySeance = new ArrayList<Seance>();
		for (Service service : listService) {
			Seance seance = service.getDailySeance();
			if(seance != null && !seance.isCanceled()) listDailySeance.add(seance);
		}
		return listDailySeance;
	}
	
	
	/**
	 * Inscription d'un client à un service
	 * 
	 * @param key_seance numero de la seance
	 * @param code_member code du membre
	 * @param comments comentaire sur l'inscription 
	 * @return Registation une instance de l'inscription faite, null sinon.
	 */
	public static Registration registerClient(String key_seance, String code_member, String comments) {
		try {
			Client client = (Client) Database.getUser(code_member);
			return client.registerToSeance(key_seance, comments);
		} catch (Exception e){
			System.out.println("Le membre est introuvable ou n'est pas un client.");
		}
		return null;
	}
	
	/**
	 * Fonction qui permet a un membre de confirme sa présence a un seance.
	 * 
	 * @param key_seance numero de la seance
	 * @param code_membre code du membre
	 * @param comments commentaire de confimation
	 * @return Confirmation un instance de la confirmation réaliser, null si la confirmation n'a pas pu etre faite.
	 */
	public static Confirmation confirmPresence(String key_seance, String code_membre, String comments) {
		try {
			Client client = (Client) Database.getUser(code_membre);
			return client.confirmPresence(key_seance, comments);
		} catch (Exception e) {
			//System.out.println("Le membre n'est pas un client ou est introuvable");
		}
		return null;
	}
	
	/**
	 * Fonction qui permet de recuperer un utilisateur a partir de son code.
	 * 
	 * @param key code de l'utilisateur
	 * @return User un instance de l'utilisateur correspondant au code entré.
	 */
	public static User getUser(String key) {
		try {
			if(Database.getUser(key) != null) {
				return Database.getUser(key);
			} else {
				for(String code : Database.getListUser().keySet()) {
					User user = Database.getUser(code);
					
					if((user.getEmail().toLowerCase()).equals(key.toLowerCase())) {
						return user;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Utilisateur introuvable.");
		}
		
		return null;
	}
	
	/**
	 * Fonction qui permet de metre a jour certain paramètres d'un utilisateur
	 * 
	 * @param email le nouvel email de l'utilisateur
	 * @param phone le nouveau numero de telephone de l'utilisateur
	 * @param address la nouvelle adresse de l'utilisateur
	 * @param city la nouvelle ville de l'utilisateur
	 * @param province la nouvelle province de l'utilisateur
	 * @param postal_code le nouveau code postal de l'utilisateur
	 * @param code_membre le code du membre
	 * @return User une instance de l'utilisateur sur lequel on a fait la modification, null si le membre n'existait pas
	 */
	public static boolean editUser(String email, String phone, String address, String city, String province, String postal_code, String code_membre) {
		try {
			User user = Database.getUser(code_membre);
			for(User u : Database.getListUser().values()) {
				if((u.getEmail().toLowerCase()).equals(email.toLowerCase()) && u != user) {
					System.out.println("\nIl existe un autre compte avec cette adresse email.");
					return false;
				}
			}
			user.editUser(email, phone, address, city, province, postal_code);
			return true;
		} catch (Exception e) {
			//System.out.println("Le membre n'est pas un client ou est introuvable");
		}
		return false;
	}
	
	/**
	 * Fonction qui permet de suprimmer un utilisateur.
	 * 
	 * @param code_member code de l'utilisateur a supprimer
	 * @return true si la supression a bien été réaliser, false sinon
	 */
	public static boolean deleteUser(String code_member) {
		try {
			User user = Database.getUser(code_member);
			return user.deleteUser();
		} catch (Exception e) {
			// L'utilisateur est introuvable.
		}
		return false;
	}
	
	
	/**
	 * Fonction qui permet de modifier certain paramètre d'une séance
	 * 
	 * @param key le code de la séance
	 * @param code_pro le code du professionnel
	 * @param date la nouvelle date de la seance
	 * @param hour le nouvel horraire de la seance
	 * @param max_capacity la nouvelle capacié d'acceuil de la seance
	 * @param price le nouveau prix de la seance
	 * @param comments commentaire de la seance
	 * @return Seance instance de la seance sur laquelle on fait des modifications , null sinon 
	 */
	public static boolean editSeance(String key, String code_pro, String date, String hour, int max_capacity, double price, String comments) {
			try {
				Seance seance = Database.getSeance(key);
				try {
					return seance.editSeance(code_pro, date, hour, max_capacity, price, comments);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			} catch (Exception e) {
				System.out.println("Séance introuvable");
			}
		return false;
	}
	
	
	/**
	 * Fonction qui annule une seance
	 * 
	 * @param key numero de la seance
	 * @param pro_number numero du proffessionel
	 * @return true si l'annulation de la seance a ete fait avec succes, false si la seance n'existe pas
	 */
	public static boolean deleteSeance(String key, String pro_number) {
		try {
			Seance seance = Database.getSeance(key);
			return seance.deleteSeance(pro_number);
		} catch (Exception e) {
			System.out.println("Séance introuvable");
		}
		return false;
	}
	
	/**
	 * Fonction qui va crée le rapport comptable
	 * 
	 * @return le path ou se trouve le fichier du rapport
	 */
	public static String rapportComptable() {
		try {
			return Comptable.rapport();
		} catch (IOException e) {
			System.out.println("Erreur lors de la création du rapport. Veuillez vérifier que le dossier 'Rapports' est présent dans le dossier Implementation");
		}
		return new String();
	}
	
	/**
	 * Fonction qui permet d'acceder a une seance
	 * 
	 * @param key le numero de la seance 
	 * @return Seance instance de la seance correspondant au code entré, null sinon
	 */
	public static Seance getSeance(String key) {
		try {
			Seance seance = Database.getSeance(key);
			return seance;
		} catch (Exception e) {
			System.out.println("Séance introuvable.");
		}
		return null;
	}
	
	/**
	 * Fonction qui appelle la procedure compable
	 * 
	 * @return String contenant les details sur le deroulement de la procedure comptable
	 */
	public static String procedureComptable() {
		try {
			return Comptable.procedure();
		} catch (IOException e) {
			//System.out.println("Erreur lors de l'exécution de la procédure comptable");
		}
		return "";
	}
	
}
