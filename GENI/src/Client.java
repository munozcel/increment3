// Ryan Azoune
// Rym Bach
// Celina Ghoraieb-Munoz

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

/**
 * Modèle d'un client, extension de la classe User.
 * 
 * @author
 */

public class Client extends User {

	/**
	 *  Si state = true : Client valide
	 *  Sinon : Membre suspendu
	 */
	private boolean state;
	private ArrayList<Registration> listRegistration = new ArrayList<Registration>();
	private ArrayList<Confirmation> listConfirmation = new ArrayList<Confirmation>();
	private ArrayList<Seance> listSeance = new ArrayList<Seance>();
	
	
	/**
	 * Construction d'une instance client.
	 * 
	 * @param first_name Prénom.
	 * @param last_name Nom.
	 * @param birth_date Date de naissance.
	 * @param email Adresse email.
	 * @param phone Numéro de téléphone.
	 * @param address Adresse postale.
	 * @param city Ville.
	 * @param province Province.
	 * @param postal_code Code postal.
	 */
	public Client(String first_name, String last_name, String birth_date, String email, String phone, String address, String city, String province, String postal_code) {
		
		super(first_name, last_name, birth_date, email, phone, address, city, province, postal_code);
		this.state = true;
		
	}
	
	/**
	 * Construction d'une instance client de démonstration.
	 * 
	 * @param first_name Prénom.
	 * @param last_name Nom.
	 * @param birth_date Date de naissance.
	 * @param email Adresse email.
	 * @param phone Numéro de téléphone.
	 * @param address Adresse postale.
	 * @param city Ville.
	 * @param province Province.
	 * @param postal_code Code postal.
	 * 
	 * @param code_demo Numéro de démonstration du client.
	 */
	public Client(String first_name, String last_name, String birth_date, String email, String phone, String address, String city, String province, String postal_code, String code_demo) {
		
		super(first_name, last_name, birth_date, email, phone, address, city, province, postal_code, code_demo);
		this.state = true;
		
	}

	/**
	 * Suspend le client.
	 */
	public void suspendClient() {
		this.state = false;
	}
	
	/**
	 * Passe l'état du client à valide.
	 */
	public void validClient() {
		this.state = true;
	}
	
	/**
	 * Inscrit le client à une seance.
	 * 
	 * @param code Numéro de séance.
	 * @param comments Commentaire de l'inscription.
	 * @return Registration Instance de l'inscription contenant les détails de celle-ci. Si l'inscription echoue, retourne null.
	 */
	public Registration registerToSeance(String code, String comments) {
		Seance seance = Database.getSeance(code);
		
		try {
			if(seance.register(this)) {
				
				Registration reg =new Registration(seance.getDate(), seance.getPro(), this.getNumber(), seance.getServiceCode(), comments);
				listRegistration.add(reg);
				this.listSeance.add(seance);
				
				return reg;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return null;
	}
	
	/**
	 * Confirme l'inscription du client à une séance.
	 * 
	 * @param key Numéro de la séance.
	 * @param comments Commentaire de la confirmation.
	 * @return Confirmation Instance de la confirmation de l'inscription. Null si la confirmation échoue.
	 */
	public Confirmation confirmPresence(String key, String comments) {
		try {
			Seance seance = Database.getSeance(key);
			
			if(seance.containsUser(this)) {
				
				if(seance.isCanceled()) {
					System.out.println("La séance à été annulée");
					return null;
				}
				
				Confirmation confirm = new Confirmation(seance.getPro(), this.getNumber(), seance.getServiceCode(), comments);
				listConfirmation.add(confirm);
				return confirm;
				
			} else {
				System.out.println("L'utilisateur n'était pas inscrit à la séance");
				return null;
			}
			
		} catch (Exception e) {
			System.out.println("La séance est introuvable");
		}
		
		return null;
	}
	
	/**
	 * Echange l'état du client.
	 */
	public void switchState() {
		this.state = !(this.state);
	}
	
	/**
	 * Supprime le client et le désincrit de toutes les séances auxquels il été inscrit.
	 * 
	 * @return boolean true lorsque le client à été supprimé.
	 */
	@Override
	public boolean deleteUser() {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date();
		LocalDate date = LocalDate.parse(dateFormat.format(d));
		
		for(Seance seance : this.listSeance) {
			if(seance.getDate().isAfter(date)) return false;
			else {
				seance.deleteUser(this);
			}
		}
		
		Database.deleteUser(this.getNumber());
		return true;
	}
	
	/**
	 * Retourne l'état du client.
	 * 
	 * @return boolean
	 */
	@Override
	public boolean getState() {
		return this.state;
	}
	
}
